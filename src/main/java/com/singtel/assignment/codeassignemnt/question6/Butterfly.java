package com.singtel.assignment.codeassignemnt.question6;

import com.singtel.assignment.codeassignemnt.question1.Bird;
import com.singtel.assignment.codeassignemnt.question1.Flyable;

public class Butterfly extends Bird implements Flyable{

	public void fly() {
	    System.out.println("Butterfly can fly");
	}


}
