package com.singtel.assignment.codeassignemnt.question6;

public class Solution6 {

    public static void main(String[] args) {

	Butterfly butterfly = new Butterfly();
	butterfly.fly();

	Caterpillar caterpillar = new Caterpillar();
	caterpillar.walk();

    }

}
