package com.singtel.assignment.codeassignemnt.question1;

public class Solution1 {

	public static void main(String[] args) {
		Bird bird = new Bird();
		bird.walk();
		bird.fly();
		bird.sing();
	}

}
