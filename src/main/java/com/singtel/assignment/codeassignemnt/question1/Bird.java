package com.singtel.assignment.codeassignemnt.question1;


public class Bird extends Animal implements Flyable,Singing {

	public void fly() {
		System.out.println("I am flying");
	}
	
	public void sing() {
		System.out.println("I am singing");
	}

}
