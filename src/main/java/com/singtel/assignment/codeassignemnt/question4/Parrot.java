package com.singtel.assignment.codeassignemnt.question4;

import com.singtel.assignment.codeassignemnt.question1.Animal;

public class Parrot extends Animal1 implements Sound {

	private String address;
	private Animal1 companion;
	
	public Parrot() {
		
	}

	Parrot(String address, Animal1 companion) {
		this.address = address;
		this.companion = companion;
	}
	
	Parrot(String address, Sound sound) {
		this.address = address;
		this.companion = companion;
	}

	public void sound(Animal1 Companion) {
		companion.sound();
	}
	
	public void sound(Phone phone) {
		phone.sound();
	}

	@Override
	public String toString() {
	    return String.format("Parrot [address=%s, companion=%s]", address, companion);
	}

}
