package com.singtel.assignment.codeassignemnt.question4;

public class Solution4 {

    public static void main(String[] args) {

	Animal1 dog = new Dog();
	Parrot parrotWithDog = new Parrot("house", dog);
	parrotWithDog.sound(dog);

	Animal1 cat = new Cat();
	Parrot parrotWithCat = new Parrot("house", cat);
	parrotWithCat.sound(cat);

	Animal1 rooster = new Rooster();
	Parrot parrotWithRooster = new Parrot("house", rooster);
	parrotWithRooster.sound(rooster);
	
	Animal1 duck = new Duck();
	Parrot parrotWithDuck = new Parrot("house", duck);
	parrotWithDuck.sound(duck);
	
	Phone phone = new Phone();
	Parrot parrotWithPhone = new Parrot("phone", phone);
	parrotWithPhone.sound(phone);

    }

}
