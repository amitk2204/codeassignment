package com.singtel.assignment.codeassignemnt.question5;

public class Fish implements Swimmable{

	private String size;
	private String colour;

	public Fish(String size, String colour) {
	    this.size = size;
	    this.colour = colour;
	}

	public Fish() {
		super();
	}

	public void swim() {
		System.out.println(" I can swim");
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

}
