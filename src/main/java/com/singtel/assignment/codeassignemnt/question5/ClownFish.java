package com.singtel.assignment.codeassignemnt.question5;

import com.singtel.assignment.codeassignemnt.question1.Singing;

public class ClownFish extends Fish implements Swimmable, Singing {

	
	
    public ClownFish() {
		super();
	}

	public ClownFish(String size, String colour) {
	super(size, colour);
    }

    public void swim() {
	System.out.println("Shark Can swim");
    }

    public void sing() {
	System.out.println("Make Jokes");
    }

}
