package com.singtel.assignment.codeassignemnt.question5;

import com.singtel.assignment.codeassignemnt.question1.Singing;

public class Shark extends Fish implements Swimmable, Singing {

    public Shark(String size, String colour) {
	super(size, colour);
    }

    
    public Shark() {
		super();
	}


	public void swim() {
	System.out.println("Shark Can swim");
    }

    public void sing() {
	System.out.println("Make Jokes");
    }

    public void eat() {
	System.out.println("Shark eat Other Fish");
    }

}
