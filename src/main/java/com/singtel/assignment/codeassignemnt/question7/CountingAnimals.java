package com.singtel.assignment.codeassignemnt.question7;

import com.singtel.assignment.codeassignemnt.question1.Animal;
import com.singtel.assignment.codeassignemnt.question1.Bird;
import com.singtel.assignment.codeassignemnt.question1.Flyable;
import com.singtel.assignment.codeassignemnt.question1.Frog;
import com.singtel.assignment.codeassignemnt.question1.Singing;
import com.singtel.assignment.codeassignemnt.question5.Swimmable;
import com.singtel.assignment.codeassignemnt.question2.Chicken;
import com.singtel.assignment.codeassignemnt.question2.Duck;
import com.singtel.assignment.codeassignemnt.question3.Rooster;
import com.singtel.assignment.codeassignemnt.question4.Cat;
import com.singtel.assignment.codeassignemnt.question4.Dog;
import com.singtel.assignment.codeassignemnt.question4.Parrot;
import com.singtel.assignment.codeassignemnt.question5.ClownFish;
import com.singtel.assignment.codeassignemnt.question5.Dolphin;
import com.singtel.assignment.codeassignemnt.question5.Fish;
import com.singtel.assignment.codeassignemnt.question5.Shark;
import com.singtel.assignment.codeassignemnt.question6.Butterfly;

public class CountingAnimals {

	public static void main(String[] args) {
		
		Animal[] animals = new Animal[] { new Bird(),
				new Duck(),
				new Chicken(),
				new Rooster(),
				new Parrot(),
				new Frog(),
				new Dog(),
				new Butterfly(),
				new Cat() 
			};
		
		Swimmable[] swimmable = new Swimmable[] { 
				new Fish(),
				new Shark(),
				new ClownFish(),
				new Dolphin()
			};

		
		int sing = 0;
		int fly = 0;
		int walk = 0;
		int swim = 0;

		for (Animal animal : animals) {
			if (animal instanceof Animal) {
				walk = walk + 1;
			}
			if (animal instanceof Singing) {
				sing = sing + 1;
			}
			if (animal instanceof Flyable) {
				fly = fly + 1;
			}
		}

		for (Swimmable swimmable2 : swimmable) {
			if (swimmable2 instanceof Swimmable) {
				swim = swim + 1;
			}
		}
		System.out.println("No. of Animas Can walk: " +walk);
		System.out.println("No. of Animas Can sing: " +sing);
		System.out.println("No. of Animas Can fly: " +fly);
		System.out.println("No. of Animas Can swim: " +swim);
	}

}
