package com.singtel.assignment.codeassignemnt.question2;

public class Solution2 {

	public static void main(String[] args) {
	    	Chicken chicken = new Chicken();
	    	chicken.sing();
		Duck duck = new Duck();
		duck.swim();
		duck.sing();
	}

}
