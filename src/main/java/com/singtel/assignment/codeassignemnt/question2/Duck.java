package com.singtel.assignment.codeassignemnt.question2;

import com.singtel.assignment.codeassignemnt.question1.Bird;
import com.singtel.assignment.codeassignemnt.question1.Flyable;
import com.singtel.assignment.codeassignemnt.question1.Singing;

public class Duck extends Bird implements Swimmable, Singing {

    public void sing() {
	System.out.println("Duck say Quack Quack");
    }

    public void swim() {
	System.out.println("Duck can swim");
    }

}
