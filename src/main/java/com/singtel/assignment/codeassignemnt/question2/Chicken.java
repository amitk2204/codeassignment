package com.singtel.assignment.codeassignemnt.question2;

import com.singtel.assignment.codeassignemnt.question1.Animal;
import com.singtel.assignment.codeassignemnt.question1.Singing;

public class Chicken extends Animal implements Singing {

    public void sing() {
	System.out.println("Chicken say cluck cluck");
    }
}
