package com.singtel.assignment.codeassignemnt.question3;

import com.singtel.assignment.codeassignemnt.question1.Animal;
import com.singtel.assignment.codeassignemnt.question1.Singing;

public class Rooster extends Animal implements Singing{

	public void sing() {
		System.out.println("Rooster says Cock-a-doodle-doo");
	}


}
