package com.singtel.assignment.codeassignemnt;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import com.singtel.assignment.codeassignemnt.question1.Bird;

public class SolutionTest1 {

    @Test
    public void testTheBirdcanSing() {
    	Bird bird = mock(Bird.class);
    	doNothing().when(bird).sing();
    	bird.sing();
    	verify(bird,times(1)).sing();
    }
}
